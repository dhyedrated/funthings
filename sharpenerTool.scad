razorW= 23;
razorT=.5;
razorL=5;
holePL = 9;
holePZ=2; //from the top
holeOD =3;
holeID=1.5;

handleW = razorW+25;
handleT =3;
/*
difference(){
    cylinder(d=3
}*/

difference() {
    cube([handleW,handleT, razorL+2]);
    cube([razorW,razorT, razorL]);
        translate([holePL,razorT,razorL-holePZ]) 
    rotate([-90,0,0]) {
            cylinder(d=holeID, 4, $fn=5);
    }
}   