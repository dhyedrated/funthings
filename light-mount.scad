lampD = 26.4;
wall =3;
strut = 9;
powerD = 3/8 * 25.4;
screwD=10;
armD = .5 *25.4;
frame = lampD; //use with elegant..     = 30;
//use with elegant..        frameH = frame/2;
frameH = wall;
union() {
    translate([0,0,0])//frameH/2])
    {
        difference() {
            cylinder(d=lampD+wall, h=12, $fn=36);
            translate([0,0,3])
                cylinder(d=lampD, h=12, $fn=36);
            
            translate([0,0,0])
            cylinder(d=screwD, h=5, $fn=36);
        }
    }
    //elegantButWeakFrame();
    translate([lampD/1.5,0,frameH/2]) {
        difference() {        
            cube([frame,frame, wall], center=true);
            translate([(frame/4+armD/4)/1.7,0,-frameH/2])
                cylinder(d=armD, h=frameH, $fn=36);
        }
    }
        
        
    
}

module elegantButWeakFrame() {
    difference() {
        cube([frame,frame, frameH], center=true);
        cube([frame-strut, frame, frameH-wall], center=true);
        cube([frame,frame-strut,  frameH-wall], center=true);
    //    translate([frame/4,0,-frameH/2])
      //      cylinder(d=powerD, h=10, $fn=36);
        translate([0,0,-frameH/2])
            cylinder(d=armD, h=frameH, $fn=36);
    }
}