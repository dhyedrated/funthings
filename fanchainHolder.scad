fanWdt = 6;
fanDpt = 50;
fanHgt = 30;
wall = 3;
chain =8;
toothW = 5;
width = 2*wall+fanWdt;
dpt =fanDpt-2*wall;
hgt =  2*fanHgt;
            //cube([fanWdt,fanHgt,fanHgt]);

difference() {
    cube([width, dpt,hgt]);
    translate([fanWdt*.5,-1,-1]) {
        cube([fanWdt, fanDpt, fanHgt]);
    
        translate([-fanWdt,dpt*.522,fanHgt*1.375]) {
      
            rotate([45,0,0]) {
                cube([fanWdt*4,fanHgt,fanHgt]);
            }
            
            //rotate([0,0,90]) {
            translate([0,0,wall*2]){
                cube([fanDpt,chain,  fanHgt],true);
            }
        }
    }
}
translate([width/4,dpt*.442,hgt*.5]) {
    cube([toothW,toothW,toothW]);
}
//*/
/* wanted to make a tooth to engage the chain, but this difference isn't rendering
difference() {
    translate([100,fanDpt*.5,fanHgt*.5]) { 
        translate([0,-fanDpt*.5,-1]) {
            cube([fanWdt, fanDpt, fanHgt]);
        }    
        rotate([45,0,0]) {
            cube([fanWdt,fanHgt,fanHgt]);
        }
    }
}
*/