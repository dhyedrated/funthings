ppBallD = 40;
ppRad = ppBallD/2;
margin =10;
wall = ppBallD +4;

pHgt =ppRad -3;

postPos = ppRad-3;

//translate
topPosts();
translate([50,0,0]) {
    bottom();
}
module topPosts() {
    
    difference() {
        union() {
            translate([0,0,ppRad])
                cylinder(d=margin+3, h=ppRad, $fa=5);
            top();
        }
       translate([postPos,postPos, -5]) {
            cylinder(d=margin -3, h=pHgt, $fa=5);
        }
       translate([-postPos,-postPos, -5]) {
            cylinder(d=margin -3, h=pHgt, $fa=5);
        }
       translate([-postPos,postPos, -5]) {
            cylinder(d=margin -3, h=pHgt, $fa=5);
        }
       translate([postPos,-postPos, -5]) {
            cylinder(d=margin -3, h=pHgt, $fa=5);
        }    
       translate([0,-0, 0]) {
            cylinder(d=margin -3, h=pHgt*3, $fa=5);
        }  
        
    }
}
module bottom() {
    difference() {
        translate([0,0,ppRad/3]) {
            bot();
        }
       translate([postPos,postPos,-5] ) { //ppRad,ppRad, -5]) {
            cylinder(d=margin -3, h=25, $fa=5);
        }
       translate([-postPos,-postPos, -5]) {
            cylinder(d=margin -3, h=25, $fa=5);
        }
       translate([-postPos,postPos, -5]) {
            cylinder(d=margin -3, h=25, $fa=5);
        }
       translate([postPos,-postPos, -5]) {
            cylinder(d=margin -3, h=25, $fa=5);
        }
        translate([0,0,-pHgt])
            cylinder(d=margin*2, h=pHgt*3, $fa=5); 
    }
}
module bot() {
    rotate([180,0,0])
    {
        translate([0,0,-ppRad/3]) {
            difference() {
                top();
                translate([-wall/2, -wall/2, 0])
                    cube([wall, wall, ppRad/3]);
            }
        }
    }
}
module top() {
    difference() 
    {
        translate([-wall/2,-wall/2,0]) {
            cube([wall,wall,ppRad]);
        }
        sphere(r=ppRad, $fa=5);
    }
}