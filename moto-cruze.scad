
//phone
coef = 1;
pX = 144.3 * coef;
pY = 73* coef;
pZ = 15.5* coef;

//frame
fudge  = 1;
fX = 7.25 * 25.4 * fudge;
fY = 3 * 25.4 * fudge;
fZ = 2; //pZ * 1.0* fudge;
// flex bar
fbX = 1;
fbY = pY;
fbZ = pZ;
    deltaX = 20;
    ySpace = 60;
/*
translate ([0,-ySpace/2,-30])
    cube([deltaX,ySpace,pZ]);
    //*/
    union() {
leafSpring(1.0 + deltaX/ySpace); //3 wasn't bad
        translate([-fY*.77,-32])
            cube([fY,2.5,pZ]);
    }
    /*
translate([0,70,0]) {
    leafSpring(2);
    translate([0,70,0]) {
        leafSpring(1.0 + deltaX/ySpace);    
    }
}*/
module leafSpring(diaCoef) {
    diameter = ySpace*diaCoef;
     intersection() {
        translate([0,-ySpace/2,0]) {
            cube([deltaX,ySpace,pZ]);
        }
        translate([diameter/2,0,0]) {
        difference () {
            cylinder(d=diameter, h=pZ, $fn=36);//, $center=false);
            cylinder(d=diameter-3, h=pZ, $fn=36);//, $center=false);
        }
        }
    }
}
module leafSpring1() {
    diameter = ySpace;
    intersection() {
        translate([0,-ySpace/2,0]) {
            cube([deltaX,ySpace,pZ]);
        }
        translate([diameter/2,0,0]) {
        difference () {
            cylinder(d=diameter, h=pZ, $fn=36);//, $center=false);
            cylinder(d=diameter-3, h=pZ, $fn=36);//, $center=false);
        }
        }
    }
}

module attemptAlpha() {
union() {
   /* difference () {
        cube([fX,fY,fZ], center =true);
        cube([pX,pY, pZ], center =true);
    }
    translate ([fX/2, 0, pZ/2])         
        cube([fbX, fbY, fbZ], center =true);
    */
   // translate ([-fX/2, 0,  pZ/2])         
    //    cube([fbX, fbY, fbZ], center =true);
    
    //translate ([pX/2, 0, pZ/2])         
   //     cube([fbX, fbY, fbZ], center =true);
    
   // translate ([-pX/2, 0, pZ/2])         
   //     cube([fbX, fbY, fbZ], center =true);
}
}




//2 hacky spacers... not quite sure how to design this 

module attemptBeta() {
    translate ([-pX/2, 0, 0])   {
    union() {
            cube([39.85/2, fbY, 2]);
            cube([fbX, fbY, fbZ]);
            translate([39.85/2,0,0]) 
                cube([fbX, fbY, fbZ]);  
    }
    }

    //phone shim
    CubePoints = [
      [  0,  0,  0 ],  //0
      [ 80,  0,  0 ],  //1
      [ 80,  15,  0 ],  //2
      [  0,  15,  0 ],  //3
      [  5,  5,  10 ],  //4
      [ 75,  5,  10 ],  //5
      [ 75,  10,  10 ],  //6
      [  5,  10,  10 ]]; //7
      
    CubeFaces = [
      [0,1,2,3],  // bottom
      [4,5,1,0],  // front
      [7,6,5,4],  // top
      [5,6,2,1],  // right
      [6,7,3,2],  // back
      [7,4,0,3]]; // left
      
    polyhedron( CubePoints, CubeFaces );
}