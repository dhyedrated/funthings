box2mm = .25 * 25.4; // quarter inch graph paper

houseH =  11 * box2mm;
houseW = 10 * box2mm;
houseD = 5 * box2mm;

chimH =  4* box2mm;
chimW = 2* box2mm;
chimD = 2 * box2mm;

chimPosZ = houseH -.5;
chimPosX = 2 * box2mm;
chimPosY = 2 * box2mm;

doorH = 5 * box2mm;
doorW = 3 * box2mm;

doorX = 3 *box2mm;
wall =3;

difference () {
//union(){
    union() {
        translate([houseW/2, houseD/2, houseH/2]) {
            house();    
        }
        
        translate([chimPosX+chimW/2,chimPosY+chimD/2, chimPosZ+chimH/2]) {
            chim();
        }
    }
    translate([doorX,0,2]) {
        cube([doorW, 3, doorH]);
    }
}

module house() {
    difference() {
        cube([houseW, houseD, houseH], center = true);
        cube([houseW-wall, houseD-wall, houseH-wall], center = true);
        
        // chim hole
       // translate([chimPosX+chimW/2,chimPosY+chimD/2, chimPosZ+chimH/2]) {
       //      cube([chimW-wall,chimD-wall, 20], center=true);
       // }
    }
}
module chim() {
    difference() {
        cube([chimW,chimD, chimH], center=true);
        cube([chimW-wall,chimD-wall, chimH], center=true);
    }
}