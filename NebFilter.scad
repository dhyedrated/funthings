tweak = 0.97;//97;
diameter = 18 * tweak;
inHole = 13 * tweak;
funDiameter = 29 * tweak;
depth = 10; // * tweak;
wall = 3;
filterStack = 5;
union() {    
  //  translate([0,0,filterStack]) {
        
        difference() {
            cylinder(d1=funDiameter, d2=diameter, h=depth/2);        
            translate([0,0,-1]) {
                cylinder(d1=funDiameter-wall, d2=diameter-wall, h=depth/2+2);
            }                
        }
        translate([0,0,depth/2]) {
            difference() {
                cylinder(d=diameter, h= depth);
                translate([0,0,-1]) {
                    cylinder(d=inHole, h=depth+2);
                 }
            }
        }
  //  }
}
capOD = funDiameter*1.03+wall;///2;
capID = funDiameter*1.03; //-wall;
translate([50,0,0*-filterStack]) {
    union() {
        translate([0,0,wall/2/2]) {
            cube([capOD - 0.5, wall, wall/2], center=true);
            cube([wall,capOD - 0.5, wall/2], center=true);
        }
        difference() {
            
            cylinder(d=capOD, h=filterStack+wall);
            translate([0,0,-1]) {
                
                 cylinder(d=capID, h=filterStack+wall+2);   
            }
        }
    }
}