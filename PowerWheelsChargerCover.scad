translate([0,-14,0 ]) {
difference() {
    cube([34,48,3], center=true);
    translate([0,7,0]) {
        translate([17,0,-2])
            cylinder(d=15, h=5);
        translate([-17,0,-2])
            cylinder(d=15, h=5);
    }
}
}