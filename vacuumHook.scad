adapterID = 30;
wall = 5;
adapterOD = adapterID + wall;
adptH = 25;
soundCoef = 2.0;
muffD = adapterID * soundCoef;
muffL = adapterID * soundCoef;

//translate([-50,150,0])
  //need to rotate?  
  //cylinder(d1=adapterID+5, d2=adapterID, h=adptH+3);
  difference () {
     // union() {
        elbow(0);
          //intersection() {  this was all an attempt to amake a lip...
         //     scale([1.1,1.1,0]) {
          //        elbow();
        ///  /    }
         //     translate([0,0,-adapterOD/2])//-adapterOD*2,-adapterOD
           //       cube([adapterID*5,adapterID*5,adapterOD], center =true);
              
          }
      //}
      translate([0,0,-adapterOD/2])//-adapterOD*2,-adapterOD
        cube([adapterID*5,adapterID*5,adapterOD], center =true);
  }
 /* translate ([175,0,3]) {
      elbow(3);
  }*/
//muffler();

module muffler() {
    difference() {
        union() {
            translate([0,0,(muffL+(wall*2))/2]) {
                resonation();
            }
            translate([adapterOD*.75,0 ,wall])
                cylinder(d=adapterOD+wall, h=muffL/2);
            translate([-adapterOD*.75,0,muffL/2+(2*wall)])
                cylinder(d=adapterOD+wall, h=muffL/2);
        }
        translate([adapterOD*.75,0 ,-wall])
            cylinder(d=adapterOD, h=muffL/2+(2.2*wall));
        translate([-adapterOD*.75,0,muffL/2+(wall)])
            cylinder(d=adapterOD, h=muffL/2+(2*wall));
    }
}

module resonation() {
    difference() {
        linear_extrude(height = muffL+(wall*2), center = true, convexity = 10, $fn=100)
        scale([1.5,1])circle(d=muffD+(wall*2));
        linear_extrude(height = muffL, center = true, convexity = 10, $fn=100)
        scale([1.5,1])circle(d=muffD);              
    }

}

module  elbow(tweak) {
    union() {
  // /*   
        rotate([90,0,0]){
            translate([adapterOD,0,0]){
                difference(){
                    cylinder(d=adapterOD, h=adptH*2);
                    cylinder(d2=adapterID+3, d1=adapterID, h=adptH*2+3);
                }
            }
            translate([-adapterOD,0,0]){
                difference(){
                    cylinder(d2=adapterOD+10,d1=adapterOD, h=adptH);
                    cylinder(d2=adapterID+13, d1=adapterID, h=adptH+3);
                }
            }
        }//*/

     ///* 
        difference(){
            rotate_extrude(angle=180, convexity = 10)
            translate([adapterOD, 0, 0])
            circle(r = adapterOD/2);
                    
            rotate_extrude(angle=180, convexity = 10)
            translate([adapterOD, 0, 0])
            circle(r = adapterID/2);
        }
    }//*/
    
    
}