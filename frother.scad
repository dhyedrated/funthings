in2mm = 25.4;

h = 6*in2mm;
iDia = 1*in2mm;
wall =3;
union() 
{
   // intersection() {
    //    coney(false);
    rotate([-1,0,0]) {
        translate([0,-20,h/1.3/2]) {
            cube([h/2,8, h/1.3], center=true);
        }
    }
   // }
    difference() {
        coney(true);
      /*  rotate([-1,0,0]) {
            translate([0,-30,48]) {
                cube([h,20, 100], center=true);
            }
        }*/
    }
}
module coney(hollow) {
    difference() 
    {
        cylinder(d1=h/2, d2=iDia +2*wall, h=h,$fa=10);
        if (hollow ==true)
            cylinder(d1=h/2-2*wall, d2=iDia, h=h+5);
        translate([0,0,h+wall]){//+iDia]){///2]) {
            sphere(d=iDia+1.5*wall,$fa=10);
        }
        translate([0,h/6,h/2]) {
            cube([iDia, iDia, h+1],center=true);
        }
        translate([0,-h/6,h/2.25]) {
            //cube([iDia, iDia, h/1.5],center=true);  //back punchout
        }
        if (hollow ==false) {
            translate([0,h/5,h/2.25]) {
                cube([h/2, h/2, h/1.5],center=true);
            }
        }
        else {
            translate([0,h/5,h/2.25]) {
                cube([h, h, h/1.5],center=true);
            }
        }
    }
}