//chair mount


w= 30;  //this value doesn't really matter
side = 26;  // probably shoul dbe closer to 7/8
wall = 3;
sqrt2= 1.4142136; //assumes isocelces rTriange
/*
chair has a sq/rect cross section
            then we need some diagonal support st
            -ructure
    _____
    |   |-\
    |   |==\
    |   |___\
*/
difference ()
{
    union() {
        difference() {
            cube([side+2*wall,side+wall, w]);
            translate([wall,0,0])
                cube([side,side, w]);
            
        }
        translate([side+2*wall,-1,0]) 
            triangle(w);
    }
    translate([1.5*side,0.5*side,w*0.5]) {
    rotate([30,90,0]) {
        cylinder(d=5, h=side*3, center=true, $fn=36);
    }
    }
}
module triangle(isoSide) {
    /*B
        |\
        | \
    0,0 |__\
            A
    
    */
    
   linear_extrude(w) {
    triangle_points =[[0,0],[isoSide,0],[0,isoSide],[wall,wall],[isoSide-2*wall,wall],[wall,isoSide-2*wall]];
    //                  A,B
    triangle_paths =[[0,1,2],[3,4,5]];
    polygon(triangle_points,triangle_paths,5);
   }
}