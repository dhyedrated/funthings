//minmum hallway
translate([0,79,-3]) 
{
    cube([60,38,2]);
}
    

//sinkDrain();
//toiletDrain(); 
//window();

//HVAC block
translate([0,120,2]) {
     cube([72,36,98]);
}
translate([0,0,-2]) 
{
 /*   color("blue", 1.0) 
    {
        
        cube([30,40,1]);
    }*/
    
}
infra();
sinkTShower();

///*
translate([0,-100,0])
lavishWin();  //might need to move this over to the negative x
//*/
module sinkTShower() 
{
    width = 72;// 75
    sinkCabinet();
    translate([55,0,0]) 
    {//S wall
        cube([4, width,94]);
    }
    translate([-38,0,-1.5]) 
    {//shower foot
        cube([36,width,1]);
    }
    //n wall
    translate([-42,0,0]) 
    {        
        cube([4,width,94]);
        //e wall
        hallWall(width, 97,60,30 );
    }
   //translate([-24,0,0]) 
   //{
        rotate([0,0,90]) {
            translate([-2,7,0]) 
                shower();
        }
   // }
    
}
module lavishWin() 
{
    sinkCabinet();
    translate([54,0,0]) 
    {//Shower pieces
        cube([4, 24,94]);
        translate([3,12,72])
        {
            rotate([0,105,0]){
            color("blue", 1.0)            
                cylinder(d1=1, d2=15, h=5, $fn=20);
            }
        }
        translate([3,8,40]) 
        {
            rotate([0,90,0])
                cylinder(d=1, h=3, $fn=20);
            translate([0,8,0])
                rotate([0,90,0]) 
                    cylinder(d=1, h=3, $fn=20);
            
        }
    }
    //WALLS
    translate([-4,0,0])
        cube([4,48,94]);
    translate([100,0,0]) 
    {
        cube([4, 48,94]);
    }
    hallWall(48,100, 26,30);
   /* difference() 
    {//WAll with door hole
        
        translate([0,48,0])
        {        
            cube([100,4,94]);
        }
        translate([26,47,0]) 
        {
            cube([30,6,72]);
        }
    }
    translate([56, 50,-1]) 
    {//Door Swing
        intersection() {
           
            translate([0,15,0]) 
            {
                color ("red")
                square(size=[60,30], center=true );
            } 
            union() {
                color ("red"){
                    circle(r=30);
                }
                circle(r=2);
            }
        }
    }*/
}
module shower() {
    translate([3,12,72])
        {
            rotate([0,105,0]){
            color("blue", 1.0)            
                cylinder(d1=1, d2=15, h=5, $fn=20);
            }
        }
        translate([3,8,40]) 
        {
            rotate([0,90,0])
                cylinder(d=1, h=3, $fn=20);
            translate([0,8,0])
                rotate([0,90,0]) 
                    cylinder(d=1, h=3, $fn=20);
            
        }
}
module hallWall(y, l, doorOff, dw)
{
     difference() 
    {//WAll with door hole
        
        translate([0,y,0])
        {        
            cube([l,4,94]);
        }
        translate([doorOff,y-1,0]) 
        {
            cube([dw,6,72]);
        }
    }
    translate([doorOff+dw, y+2,-1]) 
    {//Door Swing
        intersection() {
           
            translate([0,15,0]) 
            {
                color ("red")
                square(size=[60,30], center=true );
            } 
            union() {
                color ("red"){
                    circle(r=30);
                }
                circle(r=2);
            }
        }
    }
}
module infra() 
{
    toiletSpec();
    translate([15,13,0]) 
    {
        toiletDrain();
        
    }

    translate([48, 5, 0]) 
    {
        sinkDrain();
    }

        translate([-100,-5,0])
            cube([100,5,94]);
//Wall with Window hole
    difference() 
    {
        
        translate([0,-5,0])
            cube([100,5,94]);
        translate([68,-5,72]) 
        {
           window();
        }
    }
}
module sinkCabinet()
{
    translate([30,0,0]) 
    { //Sink Cabinet
        difference() 
        {
            cube([24,18, 36 ]);
            translate([1,1,1])
                cube([22,18, 34]);
        }
    }
}
module window() {
    depth = 6.5;
    cube([30,depth, 18.5]);
        
}

module toiletDrain() 
{
    cylinder(d=3.5, h=7, $fn=20);
}
module sinkDrain() 
{
    cylinder(d=2.5, h=8, $fn=20);
}
module toiletSpec() 
{
    translate([-3.5/2,0,-1.75])
        cube([30+3.5,36,1]);
}