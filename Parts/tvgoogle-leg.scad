// feet for Google TV
//mounting space is rouhgly 35mm sq ( a little bigger)
//not planning on modeling in screw holes

pad = 35+2;


difference() {
    union() 
    {
        cube([pad,pad,5]);
        
        translate([0,pad-3,2])
            rotate([60,0,0])
                cube([pad,pad,5]);
        translate([pad-3,0,2])
            rotate([0,-60,0])
                cube([pad,pad,5]);
    }
    translate([0,0,pad])
        cube([4*pad,4*pad,3], center=true);
}
