ID_in = 1.0 + 9/16;
in2mm = 25.4;
ID = ID_in * in2mm;
wall = 3;
H = 5;
OD = ID + wall;
hgt = 25;

difference() {
	cylinder(d=OD, hgt+wall,$fn=100, true);
	translate([0,0,wall*2]) {
		cylinder(d=ID, hgt + wall,$fn=100, true);
	}
	union() {
	cube([H,H*3,wall], true);
	rotate([0,0,90])
	{
		translate([H*-1,0,0])
			cube([H,H*3,wall], true);
		translate([H, 0,0])
			cube([H,H*3,wall], true);
	}
	}
}