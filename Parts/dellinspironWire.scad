/* in cm
2.5 power
3
4.2  nic
14.2 hdmi
15
17.3 usb
17.8


hgts
hdmi 1.5
nic/usb ~1

0123456789a123456789b123456789c1234     
      / \  / \                    /  \  / \  
 _____| |__| |                    |  |__| |___
/_____|-|__|-|====================|__|__|-|___\
*/

width = 18;
hgt = 15;
    fingerwall = 5;
    gprStrt = hgt -fingerwall;
//func = function (x) x * x;
//echo(func(5)); // ECHO: 25
//gripMath = function(thickness) 2*fingerwall+thickness;

module gripper(thickness) {
    
    holder = 2*fingerwall+thickness; 
    union(){
        difference() {
            cube([width,holder,gprStrt], center=true);
            translate([0,0,2.5])
                cube([width,thickness, gprStrt],center=true);
        }
        translate([0,thickness/2,(gprStrt)/2]) {
            rotate([0,90,0])
                cylinder(d=fingerwall/1.25, h=width, center=true, $fn=12);
        }
        translate([0,-thickness/2,(gprStrt)/2]) {
            rotate([0,90,0])
                cylinder(d=fingerwall/1.25, h=width, center=true, $fn=12);
        }
    }
}

    holder = 2*fingerwall+5;
    runner = 25;
//gripper(10);
union() {
    cube([width,runner, 5]);//,center=true);
    translate([width/2,runner+holder/2,gprStrt/2]) 
    {
        gripper(5); 
    }    
    //runner = runner+  10 +holder;
    translate([0,40,0]) {
       cube([width,10, 5]);//,center=true);
        translate([width/2,10+holder/2,gprStrt/2]) 
        {
            gripper(5); 
        } 
    }  
    translate([0,65,0]) {
       cube([width,100, 2.5]);//,center=true);
        translate([width/2,100+holder/2,gprStrt/2]) 
        {
            gripper(10); 
        } 
    } 
    translate([0,180,0]) {
       cube([width,10, 5]);//,center=true);
        translate([width/2,10+holder/2,gprStrt/2]) 
        {
            gripper(5); 
        } 
    }
    
    
    
}