faceX=8;
faceY=7;
faceZ= 1.5;

ledX=7;  //forledge
ledY=6;
fnLZ=2.5;

pushX= 3.2; //for pusher
pushY= 4.1;
totZ= 6.2;

union() {
    cube([faceX,faceY, faceZ],center=true);
    translate([0,0,fnLZ/2])
        cube([ledX,ledY,fnLZ],center=true);
    translate([0,0, totZ/2])
        cube([pushX, pushY, totZ],center=true);
}