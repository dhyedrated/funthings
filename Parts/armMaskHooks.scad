difference() 
{
   // cube([10,10,25]);  // main form
    translate([5.5,5.5,0]) {
        cylinder(d=10, h=20, $fn=16);
    }
    translate([4.75,-.5,5]) {
        rotate([-90,0,0])
            cylinder(d=4,h=11, $fn=12); //notch
    }
    translate([3.20,0,-1]) 
        cube([3.25,11,7]); //slot
    
    translate([5,7.75,16]) {
        cylinder(d=6.5, h=9, $fn=12);
    }
}


translate ([16,0,0]) {
    difference() 
    {
       // cube([10,10,25]);  // main form
        translate([5.5,5.5,0]) {
            cylinder(d=10, h=20, $fn=16);
        }
        translate([4.75,-.5,5]) {
            rotate([-90,0,0])
                cylinder(d=4,h=11, $fn=12); //notch
        }
        translate([3.20,0,-1]) 
            cube([3.25,11,7]); //slot
        
        translate([0,7.75,16]) {
            rotate([0,90,0]) 
                cylinder(d=6.5, h=15, $fn=16);
                
        }
    }
}

translate ([35,0,0]) {
    difference() 
    {
       // cube([10,10,25]);  // main form
        translate([5.5,5.5,0]) {
            cylinder(d=10, h=20, $fn=16);
        }
        translate([4.75,-.5,5]) {
            rotate([-90,0,0])
                cylinder(d=4,h=11, $fn=12); //notch
        }
        translate([3.20,0,-1]) 
            cube([3.25,11,7]); //slot
        
        translate([0,4.75,15.5]) {
            rotate([-45,0,0]) 
                cube([11,3.25,8]); //slot
              //  cylinder(d=6.5, h=20, $fn=16);
                
        }
    }
}


translate ([16,20,0]) {
    difference() 
    {
       // cube([10,10,25]);  // main form
        translate([5.5,5.5,0]) {
            cylinder(d=10, h=20, $fn=16);
        }
        translate([4.75,-.5,5]) {
            rotate([-90,0,0])
                cylinder(d=4,h=11, $fn=12); //notch
        }
        translate([4.20,0,-1]) 
            cube([2.25,11,7]); //slot
        
        translate([0,7.75,16]) {
            rotate([0,90,0]) 
                cylinder(d=6.5, h=15, $fn=16);
                
        }
    }
}

translate ([35,20,0]) {
    difference() 
    {
       // cube([10,10,25]);  // main form
        translate([5.5,5.5,0]) {
            cylinder(d=10, h=20, $fn=16);
        }
        translate([4.75,-.5,5]) {
            rotate([-90,0,0])
                cylinder(d=4,h=11, $fn=12); //notch
        }
        translate([4.20,0,-1]) 
            cube([2.25,11,7]); //slot
        
        translate([0,4.75,15.5]) {
            rotate([-45,0,0]) 
                cube([11,3.25,8]); //slot
              //  cylinder(d=6.5, h=20, $fn=16);
                
        }
    }
}