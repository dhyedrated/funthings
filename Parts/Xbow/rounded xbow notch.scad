in2mm = 25.4;

width = 21/32 * in2mm;
height = 24/32 * in2mm;
depth = 7/32 * in2mm;

inH = 11/32 * in2mm;
inH_2 = height - 2.5;

///Notch cutouts
notch = 1.5/32 * in2mm;
notchCut = 2.2/32 * in2mm;
cylH = 20;
notchX = 2.75/32 * in2mm;
notchY = 14/32 * in2mm;


//
union() {
XBowTip();
translate([1,height-1,depth/2])
	cylinder(r=notch, depth,center=true,$fn=12);

translate([width-1,height-1,depth/2])
	cylinder(r=notch, depth,center=true,$fn=12);
}
//translate([width,notchY+1,depth/2])
//	cylinder(r=notch, depth,center=true,$fn=12);


module XBowTip() {
	difference() {
		cube([width, height, depth]);//, center=true);
		translate([2, -0.1, 2]) {
			cube([width-4, inH, depth-4]);//, center=	true);
		}
		translate([4, -0.1, 2]) {
			cube([width-8, inH_2, depth-4]);
		}
		translate ([notchX, notchY,-.1]) {
			cylinder(r=notchCut, cylH, center = true, $fn=12);
			translate([-1.1,0,0]) {
				rotate([0,0,25]){
					cube([notchX,cylH, cylH]);
				}
			}
		}
		translate([width-notchX, notchY,-.1]) {
			cylinder(r=notchCut, cylH, center = true, $fn=12);
			translate([-.9,0.45,0]) {
				rotate([0,0,-25]){
					cube([notchX,cylH, cylH]);
				}
			}
		}

	}
}

