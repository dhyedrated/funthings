/**   9
  -----------|
5 | | 4 |  5 |
  |_|   |    |25  ~ large leg
   2    |    |
   
^^notch   
   extrude 8, or 10
*/
xtru= 8;
llX = 4;
llY = 25;

nX = 10;
nY = 5;
 
vX = 4.5;
vY= 3.5;

    difference() {
union() {
        cube([nX, nY,xtru]); 
    cube([llX,llY, xtru]);
    }
        translate([4,2.5,0]) {
            cube([vX, vY, xtru]);
        }
   
}
