wide = 9.0;
tall = 0.75;
dpth = 1;
probeD = 2.5;
probeW = 1.5;
ringID = 20;
ringOD = 25;

buffer =2;

union() {
translate([wide/2,-ringOD/2+buffer,0]) {
    difference() {
    cylinder(d=ringOD, h=tall);
        cylinder(d=ringID, h=tall);
    }
}
cube([wide, dpth+buffer, tall]);
translate([(wide-probeW)/2, 0,0])
    cube([probeW, probeD+buffer, tall]);
}