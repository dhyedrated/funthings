wide = 9.0;
tall = 0.5;
ringTall = 1.75;
dpth = 1;
probeD = 2.5;
probeW = 1.5;
ringID = 10;
ringOD = 15;

buffer =2;

union() {
    
        translate([0,-ringID+2,3.75/2.2]){ //-(0.04)]) {
    rotate([90,0,0]){
        
            headPhoneJack();    
        }
    }
translate([wide/2,-ringOD/2+buffer,0]) {
    difference() {
    cylinder(d=ringOD, h=ringTall);
        cylinder(d=ringID, h=ringTall);
    }
}
cube([wide, dpth+buffer, tall]);
translate([(wide-probeW)/2, 0,0])
    cube([probeW, probeD+buffer, tall]);
}

module headPhoneJack() {
    cylinder(d1=3.75,d2=3, h=15,$fn= 10, center=false);
}