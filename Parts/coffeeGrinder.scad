nubD = 2.3;
fudge = 0.12;
axelD = 9 + fudge;
axelHgt = 5+fudge;
nubHgt = 2;
// O            O
//|-------38-----|  
//outer edges of each nub so we need to subtract half a Diameter from each.. 
//then get the middle... the 2 halves cancel to 1
//(38 - 2*(nubD/2) )/ 2
nubPos = (38 - nubD)/2;
wall =2;
coreH= axelHgt +wall;
coreL= nubPos*2 +10;
coreW = axelD +wall;
//wing();

project();

module project() {
    difference() {
        core();
        cylinder(d=axelD, h=axelHgt, $fn=36);
    }
}

module core() {
    union() {
        translate([coreL/4,0,0]) {
            rotate([0,90,0]) {
                wing();
            }
        }
        translate([nubPos,0,-nubHgt]) {
            cylinder(d1=nubD-fudge,  d2=nubD, h=nubHgt, $fn=36);
        }
        
        translate([-coreL/4,0,0]) {
            rotate([0,90,180]) {
                wing();
            }
        }
        
        translate([-nubPos,0,-nubHgt]) {
            cylinder(d1=nubD-fudge,  d2=nubD, h=nubHgt, $fn=36);
        }
    }
       
}

module wing() {
    difference() {
        cylinder(d1=coreH*2, d2=wall, h=coreL/2, center=true, $fn=36);
        translate([coreH/2,0,0])
            cube([coreH, coreH*2, coreL/2], center=true);
    }
}