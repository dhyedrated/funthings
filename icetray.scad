//y = mx + b

//
c = 12.5*10;
bk = 12*10; //or 15.5 if rounded edges  10
xD = sqrt(pow(c,2) - pow(10.5*10,2)); 
/*    ------ft - xD
   /        |   \
  /     10.5|    \  
 /          |     \
0,0--------------ft,0
*/

ft = 25*10;
difference() {
    linear_extrude(10)
        polygon(points=[[0,0],[ft,0],[ft-xD,10.5*10],[xD,10.5*10]]);
    translate([ft/2-5*10, 5.25*10-4.5*10,5.25*10])
        cube([10*10,9*10,5*10]);
}