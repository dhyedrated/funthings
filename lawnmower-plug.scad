//tractor clip holder

buffer =2; 
d1 = 5.5;
d2 = 10.6 + buffer;
h2 = 30;
h1_2 = 50;
//

difference() {
    union() {    
       // cube([
        cylinder(d=d1, h=h1_2, $fn=30);
        translate([0,0,h1_2-h2]) {
            cylinder(d1=d1, d2=d2, h=h2+buffer, $fn=30);
            
        }
        translate([0,0,(h1_2)])
        {
                cylinder(d2=d2, d1=15.5+buffer,h=10);
               
        }
    }
//*/
    translate([0,0,30.2]){     
        cylinder(d=1.75, h=30, $fn=36);
    }
}