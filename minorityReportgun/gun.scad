in2mm = 25.4;
handleth = 0.45*in2mm;

holeDia = 0.8 * in2mm;

sz = 1.5 * in2mm;  //handle general size


    hgt = 7;
    dia =50;
    t =16;//80;
    toothEdge = dia*1.5/t;
    rat = (dia*.5)/t;

        houseR = dia*.5+toothEdge;
// house();
present(); 
//main();
module present() { /// THIS method move stuff around for positioning visualization purposes
    rotate([0,180,0]) {
        //so far i like this positioning/sizing... doubt the 
        // rotation will be satifying with a short moment arm
        // but increased weight can fix that..
        translate([-houseR,0,houseR+hgt+1]) {   
         //   barrel();
        }
        winch();
    }
    //
    union() {
        translate ([0,0,-(hgt*1.5)]) {
            translate([37,35,1]) {
                handle();
            }
            house();
        }
    }
}
//////////////////
module handle() {
    difference() {
        union() {    
            cylinder(d=sz, h=handleth);
            translate([sz/2.35, sz/2.35, 0]) {
                cylinder(d=sz, h=handleth);
            }
        }
        cylinder(d=holeDia, h=handleth);
        translate([sz/2.35, sz/2.35, 0]) {
            cylinder(d=holeDia, h=handleth);
        }
    }
}

module hinge() {
    cylinder(d=holeDia, h=handleth);
   
}
 module trigger(th) {
     OD = 30;
     ID= 25;
    // th = 7;
     union() {
         difference() {
             cylinder(d=OD, h=th);
             cylinder(d=ID, h=th+2);
             translate([0,-OD/2,0]) {
                 cube([OD/2, OD, th]);
             }
         }
         translate([-OD/2,(flgY)/2,0]) {
             rotate([0,0,-45])
                cube([th, flgY, th]);
         }
     }
 }

module main() {
    //DEPRECATED
    union() {
        handle();
        translate([sz*.6, sz,0]){
            hinge();
        }
    }
}
module siding() {
    /*
    not sure if this is going to work.. 
    got a little carried away with aestheics... 
    */
     rotate([180,0,0]) {
        difference() {
            cylinder(d1=houseR*2.2, d2=houseR, h=hgt*3, $fn=t*2);
            cylinder(d1=houseR*2.2-hgt, d2=houseR-hgt, h=hgt*3-hgt*0.5, $fn=t*2);
      }  
    }
}

    flgY = 25;
module house() { 
    
    Hhgt = hgt*3;
    wall = hgt*0.5;
    union() {
        difference() {
            difference() {
                cylinder(d=houseR*2.15,  h=Hhgt, $fn=t*2);
                cylinder(d=houseR*2.15-wall, h=Hhgt-wall, $fn=t*2);
            }
            //slices for compliant mech
            translate([-houseR*(2.15*.5), -(flgY)/2, Hhgt*0.5+toothEdge*.25]) {
                cube([wall*2,flgY,wall*0.5]);
            }
            translate([-houseR*(2.15*.5), -(flgY)/2, Hhgt*0.5-(toothEdge*.25+wall)]) {
                cube([wall*2,flgY,wall*0.5]);
            }
            //axel hole
            cylinder(d=4, h=dia, $fn=t*4);
           // translate([-houseR*(2.15*.5), (flgY)/2, (Hhgt-wall)*0.5]) {
          //      cube([wall*2,toothEdge*.25,wall*2]);
           // }
            
            translate([0,0,-26.5]) {
        //        barrel(true);            
            }
        }
        translate([-(houseR)-30, -13,(Hhgt-toothEdge)/2])
        {/// Need to hook up a trigger to flange to push it away from gear 
            //AWAY from gear
            rotate([0,0,-75])
            trigger(toothEdge);
          //  hammer();   
        }
    }
}

module hammer() {
    x = toothEdge*0.9; 
    y = toothEdge*0.5;
    z = 0.9 * hgt;
    flgX = 3;
    deg = 0;// 15;
    union() {
        cube([x, y, z]);
        rotate([0,0,-deg]) {
            translate([(-x*.5)-(flgX*(deg/90))
                    ,-flgY+y
                    ,0]) 
            {
                difference () {
                    cube([flgX,flgY,z]);
                    translate([flgX*0.5,flgY*0.1,0]) {
                        cylinder(d=flgX*0.5, h=z, $fn=36);
                    }
                }
            }
        }
    }
}


module barrel(solid =false) {
    l = dia*2;
    rotate([0,90,0]) {
        difference() {
            cylinder(d1=houseR*2.2, d2=houseR*2, h=l, $fn=t*2);
            if (solid ==true) {
                cylinder(d1=houseR*2.2-hgt, d2=houseR*2-hgt, h=l, $fn=t*2);
            }
        }
    }
}


module winch() {
    difference() {
        union() {
            toothedWheel(hgt);
            cylinder(d=8, h=dia+hgt*2);
        }
        cylinder(d=4, h=dia, $fn=t*4); 
        //this stub need to be a part of the barrel so they all spin together..
        //around the low friction axel within to the handle
        //ooo that would be a cooler trigger release assembly
    }
}

module toothedWheel(hgt) {
    union() {
        cylinder(d=dia, h=hgt, $fn=t*2);
        for(i=[0:t]) //n is number of teeth
            rotate([0,0,i*360/t])
                translate([(dia*.5)-(toothEdge*(rat*.25)),0,0]) // r is radius of circle
                    tooth(hgt,toothEdge, rat);
    }
}
module tooth(hgt,toothEdge, runVrise) {    
   linear_extrude(hgt) {       //rise            //run
    triangle_points =[[0,0],[toothEdge,0],[0,toothEdge*runVrise]];
    //,[10,10],[80,10],[10,80]];
    triangle_paths =[[0,1,2]];//,[3,4,5]];
    polygon(triangle_points,triangle_paths,10);
   }
}