adapterID = 100;
wall = 5;
adapterOD = 100 + wall;
adptH = 10;
soundCoef = 2.0;
muffD = adapterID * soundCoef;
muffL = adapterID * soundCoef;

//translate([-50,150,0])
  //need to rotate?  
  elbow();
//muffler();

module muffler() {
    difference() {
        union() {
            translate([0,0,(muffL+(wall*2))/2]) {
                resonation();
            }
            translate([adapterOD*.75,0 ,wall])
                cylinder(d=adapterOD+wall, h=muffL/2);
            translate([-adapterOD*.75,0,muffL/2+(2*wall)])
                cylinder(d=adapterOD+wall, h=muffL/2);
        }
        translate([adapterOD*.75,0 ,-wall])
            cylinder(d=adapterOD, h=muffL/2+(2.2*wall));
        translate([-adapterOD*.75,0,muffL/2+(wall)])
            cylinder(d=adapterOD, h=muffL/2+(2*wall));
    }
}

module resonation()
{
    difference() {
        linear_extrude(height = muffL+(wall*2), center = true, convexity = 10, $fn=100)
        scale([1.5,1])circle(d=muffD+(wall*2));
        linear_extrude(height = muffL, center = true, convexity = 10, $fn=100)
        scale([1.5,1])circle(d=muffD);              
    }

}

module  elbow() {
    union() {
       rotate([90,0,0]){
        translate([adapterOD,0,0]){
            difference(){
                cylinder(d=adapterOD, h=adptH);
                cylinder(d=adapterID, h=adptH+3);
            }
        }
    }

            difference(){
        rotate_extrude(angle=25, convexity = 10)
        translate([adapterOD, 0, 0])
        circle(r = adapterOD/2);
                
                rotate_extrude(angle=27, convexity = 10)
        translate([adapterOD, 0, 0])
        circle(r = adapterID/2);
            }
    }
}