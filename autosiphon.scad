bellHgt = 60; 
bellD = 40;

riserH = 20;
riserD = 10;

wall = 3;
/*
        ____________
        |           |
        |           |
        |           |
        |           |
    ____|  | |  | | |_______
    
        ____________
        |           |
        |           | |
rh      |    [ ]    | |
        |    [ ]    | |
    ____|    L--====# /_____
    
             rd
               bD/2
    
    
    
*/
tank();
pos =(bellD-riserD)/2;

translate ([pos,pos,0]) {
    //rotate([0,0,90])
        riser();
}
module tank() {
    union() {
        difference() {
            cube([bellD, bellD, bellHgt]);//, center=true);
            translate([wall, wall, 0])
                cube([bellD-2*wall, bellD-2*wall, bellHgt-wall]);//, center=true);
            
            translate([(bellD-riserD)/2, bellD-wall, 0])
            cube([riserD,wall*2,riserD]); //oh, if i make this longer in the Y.. I get a second hole..
            //wall*2, bellD*1.25, bellHgt*.25]);
            
            //translate([bellD*.6666, -wall, 0])
           // cube([wall*2, bellD*1.25, bellHgt*.25]);
           // rotate([0,0,270]) {
                translate([ -wall,bellD/3, 0])
                cube([ bellD*1.25,wall*2, bellHgt*.25]);
                
                translate([-wall,bellD*.6666,  0])
                cube([ bellD*1.25,wall*2, bellHgt*.25]);
            //}
        }
        //flap for weighing down
        translate([bellD,0,0])
            cube([bellD/2, bellD, 1]);
        translate([0,-bellD/2,0])
            cube([bellD, bellD/2, 1]);
    }
}
module riser() {
    difference() {
        union() {
            cube([riserD,riserD,riserH]);
            translate([0,riserD,0])
                cube([riserD,bellD,riserD]);
        }
        translate([wall,wall,wall]) {
                cube([riserD-wall*2,riserD-wall*2,riserH]);
                translate([0,riserD-wall*2,0])
                    cube([riserD-wall*2,bellD+riserD,riserD-wall*2]);
            
        }
    }
}