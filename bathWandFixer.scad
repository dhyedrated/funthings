//hand shower wand doesn't reliably stay off... 
//so make a clip or slip on shield... and a raceway to send 
//drips back to the tub
smg = 1.25;
wandX = 38+smg;
wandY =21+smg;
wandZ = 62;

    buff = 3;
    
//TOP PIECE
shield();
//showerWand();
// easy rectangle shape
module showerWand() {
    cube([wandX, wandY,wandZ ]);//, center=true);
}

module domeHat() {
    OR = wandZ *0.5 + 9;
    IR = OR-3;
    difference() {
        sphere(OR, $fa=6);
        sphere(IR, $fa=6);
        translate([-OR,-OR,-OR])
            cube([OR*2,OR*2,OR]);
    }
}


module cap() {
    difference() {
        cube([wandX+buff, wandY+buff/2,15+buff/2 ]);//, center=true);
        translate([1.5,1.5,-wandZ+15])
            showerWand();
    }
}

module splashGuard() {
    difference() 
    {
        cylinder(d=wandX, h=wandZ, center=true);
        translate([0,0,-2])
            cylinder(d=wandX-2, h=wandZ, center=true);
        translate([0,(wandY+buff)*-0.5,0])
            cube([wandX+buff,wandY+buff,wandZ+buff], center=true);
    }
}
//cap();
//splashGuard();
module shield() {
    union() {
        translate([(wandX+buff)*-0.5,-wandY,wandZ*0.5-(15+buff/2)]) {
            cap();
        }
        rotate([30,0,0])
            splashGuard();
    }
}
//*/
module clip() 
{
    legMaker =25;
    difference() {
        cube([wandX+buff, wandY+buff/2,15 ]);//, center=true);
            translate([1.5,1.5,-wandZ+15])
                showerWand();
        translate([(wandX+buff-legMaker)/2,0,0])
            cube([legMaker,5,15]);
    }
}
//clip();
//trough();
    OR = 70 *0.5;// + 9;
    IR = OR-1.5;

//collector();
module collector() { // this  is supposed to be a 
    // clip on catch/directer that goes behead the nozzles
    //with the intent to catch water or send it 70+ mm to the tub
    // it could slope from as high as 95 mm
    //can't decide if a dome withe a hose would be better than
    // a conicl trough
    
    union() {
        clip();
        
       /* translate([OR-(wandX-buff)/2
        , OR,15])
            dish();*/
    }
}
module trough() {
    //TODO: needs to be hollow
    cylinder(d1=3,d2=OR, h=70);
}
module dish() {
    difference() {
        sphere(OR, $fa=6);
        sphere(IR, $fa=6);
        translate([-OR,-OR, 0])//OR])
            cube([OR*2,OR*2,OR]);
    }
}
