phoneD = 8;
armW = 25;
grv = 5;
notch = 3;
armD = (grv +notch) * 2.1;
armL = 60;
barD = 25.4;
barW= 3;
mtnWidth = 70;
/////////////
barMount();
base();
//valley(7);
translate( [-55, 0,0])
//FIT POSITIONtranslate([10.5,-25,11.5])
    grip();
//slide(1);
module barMount() {  //WHERE DO I PUT THIS?
    translate([-(mtnWidth-armW)/2, 0,-7]){//-barW, -armD]) {
    rotate([0,90,0]) {
        //
   
            difference() {
                cylinder(d=barD+(barW*2), mtnWidth);
                cylinder(d=barD, mtnWidth);
            }
        }
    }
}
module slide(coef) {
    union() {
        cube([grv*coef,armL*(coef+1),grv*coef]); 
        translate([(grv*coef-notch*coef)/2,0,grv*coef])
            cube([notch*coef, armL*(coef+1), notch*coef/2]); 
    }
}

module hook(sz) {
    d = sz * .85;
    difference() {
        cube([sz, sz*1.5, sz]);
        translate([0,sz*1.5-(d/2.2),d/2])
        rotate([0,90,0])
        {            
            cylinder(d=d, h=sz);
        }
    }
}
module valley(sz, zUP) {
    if (zUP == -1) {
       // rotate([180,0,0]){
            subVal(sz);
        //}
    } else {
        subVal(sz);
    }
}
module subVal(sz) {
    d = sz/sqrt(2); //;;* .65;
    difference() {
        cube([sz, sz*1.5, sz]);
        translate([0,sz*1.5,0])//(d)/8])
            rotate([45,0,0])//,90,0])
       // {      
       // rotate([0,0,45])
        {      
            cube([sz,d,d]);//cylinder(d=d, h=sz);
       // }
    }
    }
}
module grip() {
    color([1,0,0]){
    union(){
        translate([-1,7*1.5,7+(grv)]) {
            rotate([180,0,0]) {
                valley(7, 1);
            }
            
        }
            slide(1);
        translate([-5,0,-14])
            cube([14, 14, 14]);
    }
    }
}
module base() {
    val = 7;
    union() {
        translate([armW/4-val/2,armL-val*1.5,armD-val*.4]) {
            valley(val, -1);
        }
        translate([armW-(armW/4+val/2),armL-val*1.5,armD-val*.4]) {
            valley(val, -1);
        }
        difference() {    
            cube([armW, armL,armD  ]);
            translate([(armW-grv)/2,0,armD-grv-notch/2])
                slide(1.1);
        }
    }
}
