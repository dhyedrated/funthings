dX = 2.02*25.4;  //domino inches to mm... 
dY=1.01 *25.4;
dZ = 3;

//octTrain();
//paramTrain(8); /// OSCAD variables are constants!
paramTrain(9);
module paramTrain(side) {
    step = 360/(side );
    angle = step/2;
   
    list2 = [ for (i = [angle : step : 360]) i ];
  
    
    difference() {
        cylinder(d=dX*1.75+(side * 2.54), h=dZ, $fn=36);
        rotate([0,0,angle]) {
            translate([-dX/2,-dY/2,0]) {
                cube([dX,dY,dZ]);
            }
        }
      for(lcv = [0 :1 :side-1]) {
        //echo(lcv);
        //echo(list2[lcv]);
            rotate([0,0,list2[lcv]]) {
                notch2(2/side+0.0);
            }
        }
    }  
}
module notch2(param) {
    echo(param);
    translate([dX/(1.35-param),-dY/(2-param),0]) {
        cube([dX,dY,dZ]);
    }
}
module OLDparamTrain(side) {
    step = 360/(side );
    angle = step/2;
   // list1 = [ for (i = [0 : 1 : side]) i ];
//echo(list1); 
    list2 = [ for (i = [angle : step : 360]) i ];
  //  echo(list2); 
  /*  for(lcv = [0 :1 :side]) {
        echo(lcv);
        echo(list2[lcv]);
    }*/
    
    difference() {
        cylinder(d=dX*1.75, h=dZ, $fn=36);
        rotate([0,0,angle]) {
            translate([-dX/2,-dY/2,0]) {
                cube([dX,dY,dZ]);
            }
        }
      for(lcv = [0 :1 :side-1]) {
        echo(lcv);
        echo(list2[lcv]);
          rotate([0,0,list2[lcv]]) {
                notch();
        }
        }
    }
  //  echo(angle);
  //  echo(step);    
/*  // 
        
    echo(angle);
        rotate([0,0,angle]) {
                notch();
        }
        angle = angle +step+10;
       echo(angle);
        rotate([0,0,angle]) {
            notch();
            
        }
       
      
        angle = angle +step-6;
        rotate([0,0,angle]) {
            notch();
        }
        
       /*  angle = angle +step;
        rotate([0,0,angle]) {
            notch();
        }  
        angle = angle +step;
        rotate([0,0,angle]) {
                notch();
        }
        
        angle = angle +step;
        rotate([0,0,angle]) {
            notch();
            
        }
        
        angle = angle +step;
        rotate([0,0,angle]) {
            notch();
        }
        
        angle = angle +step;
        rotate([0,0,angle]) {
            notch();
        }*/
   // }
}

module octTrain() {
    difference() {
        cylinder(d=dX*1.75, h=dZ, $fn=36);
        rotate([0,0,23]) {
            translate([-dX/2,-dY/2,0]) {
                cube([dX,dY,dZ]);
            }
        }
        
        rotate([0,0,23]) {
                notch();
        }

        rotate([0,0,68]) {
            notch();
            
        }
        rotate([0,0,115]) {
            notch();
        }
        rotate([0,0,160]) {
            notch();
        }
        
        rotate([0,0,-23]) {
                notch();
        }
        
        rotate([0,0,-68]) {
            notch();
            
        }
        rotate([0,0,-115]) {
            notch();
        }
        rotate([0,0,-160]) {
            notch();
        }
    }
}
module notch() {
    translate([dX/1.35,-dY/2,0]) {
        cube([dX,dY,dZ]);
    }
}