ball=15;
wall= 5;
pitchW = ball*10;
pitchL = ball * 25;
pitchH = ball * 2.5;

goalW = ball*5;
goalH = ball *1.5;
goalL = wall +4;

manW = ball * 0.75;
manH = goalH;
manL = wall;

rodL = pitchW*2;

module antiGOAL() {
	cube([goalW,goalL,goalH]);
}

module PITCH() {
	difference() {
	cube([pitchW+wall*2, pitchL+wall*2, pitchH+ball]);	
	translate([wall,wall,ball])
		cube([pitchW, pitchL, pitchH+3]);	
	}
}

module FOOSBOX() {
	//union() {
	difference() {
		PITCH();
		translate([(pitchW+wall*2-goalW)/2, -1, ball]) 
			antiGOAL();
		translate([(pitchW+wall*2-goalW)/2, pitchL+3, ball])
			antiGOAL();
	}
		translate([-pitchW*0.25, pitchL*0.45, pitchH*0.80])

		rotate([0,90,0]) {
				rod();
		}
	//}
}

module MAN() {
	union() {
		cube([manH, manL,manW], true);
		prism(manL, manW, manH/3);
	}
}

module prism(l, w, h){
       polyhedron(
               points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
               faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
               );
}

module rod() {
	union() {
		cylinder(d=wall*1.25, h=rodL);
		translate([0,0,rodL*0.3])
			MAN();
		translate([0,0,rodL*0.6])
			MAN();
	}
}


rod();

translate ([0, 30,0]) {
MAN();
translate ([30,0,0])
	FOOSBOX();
}

