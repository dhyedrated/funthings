
in2mm= 25.4;

module foot() {
    linear_extrude(height= 0.95 * in2mm, center = true) {
        polygon(points=[[0,0],[.9*in2mm,0],[.7*in2mm,.8*in2mm],[.2*in2mm,.8*in2mm]]);
    }
}

module shoe() {
    linear_extrude(height= 1.8 * in2mm, center = true) {
        polygon(points=[[0,0],[1.2*in2mm,0],[.9*in2mm,.85*in2mm],[.35*in2mm,.85*in2mm]]);
    }
}


difference() {
   // cube([2*in2mm, 1*in2mm, 1*in2mm], center = true);
    shoe();
    translate([.175*in2mm, 0,0]) {
        foot();
    }
    
}
