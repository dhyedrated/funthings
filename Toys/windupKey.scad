width = 2.5;
length = 20;
barrel = 25;

buffer =3;
key = 6.5;
union() {

translate([0,0,-(length+barrel)/2]) {
	cylinder(d=11,barrel, center= true);
}
translate([0,0,-(length*2+barrel+width)/2]) {
	
	cube([barrel,barrel, width], center=true);
}

cube([key,width,length], center=true);
cube([width,key,length], center=true);
cube([key-1,width-1,length+1], center=true);
cube([width-1,key-1,length+1], center=true);
cube([key-2,width-2,length+2], center=true);
cube([width-2,key-2,length+2], center=true);

	
}


module oldWay() {
difference() {
	cube([key,key, length], center=true);
	translate([width, width,0])
		cube([width,width, length+buffer], center=true);
	translate([-width, width,0])
		cube([width,width, length+buffer], center=true);
	translate([width, -width,0])
		cube([width,width, length+buffer], center=true);

	translate([-width, -width,0])
		cube([width,width, length+buffer], center=true);

}
}