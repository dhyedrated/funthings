/*
first print of barrel was terrible.. but I might have
X hop so there'ssome rough edges

barrel wall could be thinner and a little more room needed for dart

also i wiffed on the barrel lenght.. it's currently 
shorter than the dart ohhh wrong variable

*/


dartD =13;
dartC_D =4.6;
dartL = 38.2;
dartC_L = 24.6;


bDia = dartD+2;
bLen = dartC_L *2;

airDia= dartC_D-.6;
airL = dartC_L-.6;

wall=2;
resID = 16;
resOD = resID +(2*wall);
plungD = resID-1;

handleLen = resOD*2;
handleD = 10;

translate([-0,-25,0]) {
    plunger();
}
//chamber();
translate([20,20,0]) {
    preChamber();
}
/*
translate([0,0,35]) {
    fireAssembly();
}


translate([-40,0,0])
    plungerHead();
translate([0,0,0]) 
{
    resevoir();
    translate([0,0,10]) 
        plunger();
}
*/
triggerLinkage();
module triggerLinkage() {
    union() {
        catch();
        translate([0,wall,0])
            cube([bLen, wall, wall]);
        
    }
}
module catch(){
    triPoints = [[0,0],[0,wall],[wall,wall]];
    triPaths = [[0,1,2]];
    linear_extrude(wall) {
        polygon(triPoints,triPaths,10);
    }
}
module preChamber() {
    difference() {
        cylinder(d=resOD, h=2*wall, $fn=36);
        plungerRam();
    }
}
module fireAssembly() {
    preChamber();
    translate([0,0,2*wall])
        chamber();
}
module plunger() {
    union() {
        plungerHead();
        translate([0,0,-bLen/4]) {
            plungerShaft();
            
            translate([0,0,-bLen/4.5]) {
                translate([-handleLen/2,0,-3]) {
                    rotate([-0,90,0]) {
                            plungerHandle();
                    }
                }
            }
        }
    }
}

module plungerHandle() {
    difference() {
        cylinder(d=handleD, h=handleLen, $fn=36);
        translate([.5,-handleD/2,wall]) {
            cube([handleD/2, handleD, wall]);
        }
        translate([.5,-handleD/2,handleLen-(wall*2)]) {
            cube([handleD/2, handleD, wall]);
        }
    }
}

module plungerShaft() {
    union() {
        cube([plungD/2,wall,bLen/2], center= true); //shaft might need to be longer,.. but translation will too
        difference() {
            cube([wall,plungD/2,bLen/2], center= true);
            translate([-3,2, 2+wall]) {
                cube([ wall*2,wall, wall]);
            }
        }
        
    }
}

module plungerHead() {
    union() {
        cylinder(d=plungD-(wall*.5), h=wall, $fn=36);
        translate([0,0,wall]) {
            cylinder(d=plungD-(wall*1.5), h=wall, $fn=36);
            translate([0,0,wall]) {
                plungerRam();
            }
        }
    }
}
module plungerRam() {
   cylinder(d1=plungD,d2=plungD*.75, h=5, $fn=36);
}
module resevoir() {
    difference() 
    {
        union() 
        {       
            translate([handleLen/2,0,handleD/2]) {
                rotate([0,-90,0]) {
                    plungerHandle();
                }
            }
            translate([0,0,handleD/1.25]) {
                difference() {
                    cylinder(d=resOD, h=bLen/2+wall, $fn=36);
                    translate([0,0,wall])
                    cylinder(d=resID, h=bLen/2, $fn=36);
                }           
            }
        }
        scale([1.1,1.1,1])
        plungerShaft();
   }
}

module chamber() {
    difference() {
        union() {
            barrel();
            airStraw();
        }
        airHole();
    }
}
module barrel() {
    
    difference() {
        cylinder(d=bDia+(wall*2), h=bLen+wall, $fn=36);
        translate([0,0,wall]) {
            cylinder(d=bDia, h=bLen+wall, $fn=36);
        }
    }
}
module airStraw() {
    difference() {
        cylinder(d=airDia, h=airL, $fn=72);
        airHole();
    }
}

module airHole() {
    cylinder(d=airDia-wall, h=airL, $fn=72);    
}