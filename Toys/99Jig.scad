
echo(version=version());

font = "Liberation Sans:Bold";

cube_size = 60;


tweak = 1;
railW = 6.2 * tweak;

side = 34.54 * tweak;

dpt = 3;

letter_size = side * 0.66;

letter_height = dpt*2;
o = cube_size / 2 - letter_height / 2;
difference() {
    
    cube([side+ railW, side+railW, dpt*2]);
    translate([railW, railW, 0]) {
        cube([side,side, dpt]);        
		translate([16.5, 16, dpt]) //rotate([90, 0, 0]) 
        letter("87");
    }
}
/*
translate([side*2, side*2, 0]) {
    difference() {
    
    cube([side+ railW, side+railW, dpt*2]);
    translate([railW, railW, dpt]) {
        cube([side,side, dpt]);        
		translate([16.5, 16, -dpt*2]) //rotate([90, 0, 0]) 
        letter("77");
    }
}
}*/


module letter(l) {
	// Use linear_extrude() to make the letters 3D objects as they
	// are only 2D shapes when only using text()
	linear_extrude(height = letter_height) {
		text(l, size = letter_size, font = font, halign = "center", valign = "center", $fn = 16);
	}
}