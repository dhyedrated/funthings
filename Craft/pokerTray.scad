in2mm = 25.4;
shrinkage = 1.02;
height = 20;
p12dist =5.25 *in2mm * shrinkage;
p23dist = p12dist;
p34dist = (3 + 3/32)*in2mm * shrinkage;
p45dist = (6.75-1/32)*in2mm * shrinkage;
p56dist = p45dist;
p61dist = p34dist;
p1 = [0,0];
p2 = ptFromDist(0,0,tan(0),p12dist,1);
p3 = ptFromDist(p2[0],p2[1],tan(45),p23dist,1);
p4 = ptFromDist(p3[0],p3[1],tan(135), p34dist,1);
p6 = ptFromDist(p1[0],p1[1],9999, p61dist,-1);
p5 = ptFromDist(p6[0],p6[1],0,p56dist,1);
//ptFromDist(p4[0],p4[1],tan(45), p45dist,-1);
//[137,-80];//
//p6 = ptFromDist(p5[0],p5[1],0, p56dist,1);

echo (p1);
echo (tan(90));
echo (tan(0));
echo (tan(-45));
echo (tan(135));
echo (p2);

echo (p3);
echo (p4);
echo (p5);
echo (p6);
linear_extrude(height = height) {
polygon(points=[p1,p2,p3,p4,p5,p6]);
}
function ptFromDist(x, y, angle, dist, coef ) 
=[ (coef*x +(( dist/sqrt(1+pow(angle,2))))), (angle*((coef* (x + dist/sqrt(1+pow(angle,2))))-x) + (1 * y))] ;
//0.523599 r 30 degrees
//1
echo (ptFromDist(0,0,1,5,1));