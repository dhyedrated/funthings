brDia = 7;
brLen = 40;

capH = 10;

RDia = brDia *2;
RL = brLen +capH;  //NOTE THIS means we have 10mm of just plastic at the top between cap and BR,  
    // we could put 3 or 4 slices in for sliding in pennies
    // or hollow it out and put pay load on top of BR 
    //     for weight
    

union () {
    //fins
    translate ([-1.5,RDia/2,0]) {
        cube([3,capH,capH]);
    }
    rotate([0,0,120]) {
        translate ([-1.5,RDia/2,0]) {
            cube([3,capH,capH]);
        }
    }
    rotate([0,0,-120]) {
        translate ([-1.5,RDia/2,0]) {
            cube([3,capH,capH]);
        }
    }

    //Rocket
    difference() {
        union() {
            translate([0,0,RL]) {
                cylinder(d1=RDia, d2=  1, h=capH);
            }
            cylinder(d=RDia, h= RL);
        }
        
        translate([0,0,-1]) {
            cylinder(d=brDia, h=brLen+1);
        }
    }
}

