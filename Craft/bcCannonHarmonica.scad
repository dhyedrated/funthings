bcDia = 7;
bcLen = 36;

breachDia = bcDia * 2;
breachLen = bcLen + bcDia;

barrelLen = breachLen * 2;
barrelDia = bcDia * 1.25;

nubsL = breachDia + 15;
nubsD = bcDia;
/*
translate([0,0,breachLen - bcDia]) {
            rotate([90,0,0]) {
                cylinder(d=nubsD, h=nubsL, center= true);
            }
        }
*/
difference () {
difference(){
    union() {
        cylinder(d=barrelDia, h=barrelLen); //overall barrel
        cylinder(d=breachDia, h=breachLen/2);
        rotate([90,0,0]) {
            cylinder(d=breachDia, h=breachLen,center =true); //charge and shot area                
        }    
    }        
  
    translate([0,0,breachDia/8]) {
        cylinder(d=bcDia, h=barrelLen+3);  // void
    }
}
    translate([0,(bcDia/1.9),0]) {
        rotate([90,0,0]) {
            cylinder(d=bcDia, h=bcLen,center =true);  // void
        }
    }
}