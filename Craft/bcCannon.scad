bcDia = 7;
bcLen = 36;

breachDia = bcDia * 2;
breachLen = bcLen + bcDia;

barrelLen = breachLen * 2;
barrelDia = bcDia * 1.25;

nubsL = breachDia + 15;
nubsD = bcDia;



difference(){
    union() {
        cylinder(d=breachDia, h=breachLen); //charge and shot area
        cylinder(d=barrelDia, h=barrelLen); //overall barrel
        translate([0,0,breachLen - bcDia]) {
            rotate([90,0,0]) {
                cylinder(d=nubsD, h=nubsL, center= true);
            }
        }
    }    
    translate([0,0,-2]) {
        cylinder(d=bcDia, h=barrelLen+3);  // void
    }
}