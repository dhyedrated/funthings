buffer =4.0;
hgt = 4;

boardW = 105.59;
boardWLip = boardW + 2*buffer;
boardL = 39.08;
boardLLip = boardL + buffer;

pcW = 55;

holeA_offW = 30.3;
holeAC_offL = 27.1;

holeB_offL = 16.3;
holeD = -3;
holeB_offW = -38.3+holeD;

holeC_offW = -35.8+holeD;
/*
translate([-20,-20,0]) {
	union() {
		cylinder(r=buffer, buffer);
		translate([-buffer,0,0]) {
			cube([buffer*2, buffer *3, buffer]);
			
			translate([-buffer,buffer*3,0])
		cube([ buffer *4, 10, buffer*2]);
		}
	}
}//*/


cardX = 96.76+buffer*4;
cardY = 56;
cut = 2.5;
cardZMax = hgt+5.5;  //just kidding actuall z min

CubePoints =[
  [  0,  0,  cardZMax-cut ],  //0
  [ cardX,  0,  cardZMax-cut ],  //1
  [ cardX,  cardY,  cardZMax-cut ],  //2
  [  0,  cardY,  cardZMax-cut ],  //3
  [  cut,  cut,  cardZMax ],  //4
  [ cardX-cut,  cut, cardZMax ],  //5
  [ cardX-cut,  cardY-cut,  cardZMax ],  //6
  [  cut,  cardY-cut,  cardZMax ]]; //7
  
CubeFaces = [
  [0,1,2,3],  // bottom
  [4,5,1,0],  // front
  [7,6,5,4],  // top
  [5,6,2,1],  // right
  [6,7,3,2],  // back
  [7,4,0,3]]; // left

module rightAngLip() {
	
//straight edges.. needs support
	union() {
		cube([cardX,cardY,cardZMax-3]);
		translate([0,5,0])
			cube([96.76+buffer,45, hgt +16]);
	}
}

module sleeve() {
	
//straight edges.. needs support
	union() {
		cube([cardX,cardY,cardZMax-2]);
		//translate([0,5,0])
		//	cube([96.76+buffer,45, hgt +16]);
	}
}

module beveledLip() {
	union() {
		cube([cardX,cardY,cardZMax-cut]);
		translate([-buffer,0,-0.5])
		polyhedron( CubePoints, CubeFaces );
	}
}
module cardDifference() {
	sleeve();
//beveledLip();


}
module polyPractice() {
translate([-120,0,0]) {
	//thumb();

	cube([cardX,cardY,cardZMax-cut]);
 	translate([-120,0,0])
	polyhedron( CubePoints, CubeFaces );

}
}

/*
difference () {
cube([75,70,55]);
translate ([0,0,20])
	beveledLip();
}
//polyPractice();
*/

union() {

difference() {
	
	cube([boardLLip,boardWLip, hgt+3]);

	translate([buffer,buffer,hgt])
	cube([boardL,boardW, 4]);
}
translate([39.1+buffer ,22.69+buffer,0]){
	
	difference() {
		cube([96.76+buffer,56+buffer,hgt+8]);
		translate([0,buffer/2,hgt-1]) {
			cardDifference();// cube([96.76+buffer,55,hgt+3.5]);
			
			translate([0,-buffer/2,1])
				cube([6+buffer,56.88+buffer,hgt+18.5]);
			translate([84,(pcW+buffer*2)/2.25, -5]){
			thumb();
			}

		}
	}

}



/*
	translate([holeAC_offL+buffer,holeA_offW+buffer,hgt]) {
		mount();
	}


translate([holeB_offL+buffer,boardWLip+holeB_offW-buffer,hgt]) {
		mount();
	}

translate([holeAC_offL+buffer,boardWLip+holeC_offW-buffer,hgt]) {
		mount();
	}*/
}

module  thumb() {
	union() {
	cylinder(d=pcW/2.25, buffer*4, center=false);
	translate([0, -12,0])
		cube([pcW/3, pcW/2.25,buffer*4]);
	}
}

module mount() { //x,y,z) {
	translate([-1,-1,0])
		cube([5,5,1]);
}