//text("OpenSCAD");
//fc-list -f "%-60{{%{family[0]}%{:style[0]=}}}%{file}\n" | sort
difference () {
union() {
linear_extrude(10)
text("SM RT", font="Courier New:style=Bold") ;
    
    translate([-2,-2,0]) //[30,0,5])
        cube([44,12,1]);
    
}
translate([21,7,5])
    cube([12,15,2.5], center=true);
}

translate([-21,30,0]) {
union() {
    translate([-0,-30,4.5])
        cube([12,60,2], center=true);
linear_extrude(9)
text("AEIOU",font="Courier New:style=Bold", direction="ttb") ;
}
}


/*
text("SMART");
text("SMERT");
text("SMIRT");
text("SMORT");
text("SMURT");
*/