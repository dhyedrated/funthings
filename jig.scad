groove = 6.2;
sur = 35.6;
gHgt = 3;
surH = 2;

 //use <C:\Program Files\OpenSCAD\fonts\kumar-one\KumarOne-Regular.ttf>
 // translate([100,0,0])
 // text("@z",22, font="KumarOne-Regular:style=Bold");//, direction="ttb") ;
union() {
    difference() {

    union() {
        cube([groove,sur,gHgt]);
        translate([groove,0,gHgt-surH])
            cube([sur,sur,surH]);
        translate([groove, -groove,0])
            cube([sur,groove,gHgt]);
    }
        translate ([groove+1,groove+11,-1])
        {
            linear_extrude(5)
                //too thin text("44",22, font="Alef:style=Bold", "center", "center");//, direction="ttb") ;
                text("00",22, font="Bahnschrift:style=Bold", halign="left", valign="center");//, direction="ttb") ;
        }   
    
//amiri thin and elegant
    //Bahnschrift - blocky... i likeit
    //Cambria Math good one for just outlining.. thin walls
}
//   holde connector
    translate([groove+9,0,gHgt-1])
            cube([1,sur,1]);
    translate([groove+26,0,gHgt-1])
            cube([1,sur,1]);
//*/
}
/*
translate([50,0,0]) {
    difference() {

        union() {
            cube([groove,sur,gHgt]);
            translate([groove,0,gHgt-surH+1])
                cube([sur,sur,surH]);
            translate([groove-.2, -(groove-.2),0])
                cube([sur,groove,gHgt]);
        }
        translate ([groove+1,groove+11,-1])
        {
            linear_extrude(9)
                text("11",22,font="Amiri:style=Bold", halign="left", valign="center");//, direction="ttb") ;
           // font="Alef:style=Bold", "center", "center");//, direction="ttb") ;
        }
    //halign, valign, spacing,     direction, language, script)
    }
}*/