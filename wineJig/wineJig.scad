ctrHeadW =6;
ctrHeadL =31;
ctrHeadH = 12; //maybe 11
ctrArmW = 13;
ctrArmL = 66;

bottleOD =88;
neckOD =27;
neckID = 19;

btlL = 12*25.4;
wall = 3;

//cutter();//
//cutterSeat();
//bottleBase();
//bottleCup();

cutterJig();

module cutterJig() {
    difference() { // union() {
       // translate ([0,0,bottleOD/2]) {
            bottleCup();
        
        //cutterSeat();
        cutPosX = 0;
                cutPosY = -15;
                cutPosZ = 2-(bottleOD+wall+ctrHeadH)/2;
                translate([cutPosX,cutPosY,cutPosZ ])
                {
                    cutter();
                }
        //}
    }    
}

/// half circle/ band below the X plane, centered on the Y
module bottleCup() {
    rotate([0,90,0]) {
        intersection() {
            translate([0,-(bottleOD+wall)/2,-bottleOD/4]) 
            {
                cube([bottleOD,bottleOD+wall,bottleOD/2]);
            }
        
            difference() {
                bottleBase();
               
                translate([0,0,0])//wall])
                {
                    cylinder(d=bottleOD, h=ctrHeadW*4, center=true);
                }
            }
        }
    }
}
module bottleBase() {
    union(){
        translate([(bottleOD+ctrHeadW*2)/2,0,0]) 
        {
            cube([ctrHeadW*4, bottleOD+wall,ctrHeadW*4], center =true);
        }
        cylinder(d=bottleOD+wall, h=ctrHeadW*4, center=true);
    }
}
// NOT USED FOR NOW bottle cup is better
module cutterSeat() {
    difference()
    {
        translate([0,0,-0.1 *ctrHeadH])
        {
            scale([1.2,1,1.1]) 
            {
                cube([ctrHeadW, ctrHeadL, ctrHeadH], center=true);
            }
        }
        cutter();
    }
}

module cutter() {
    union() {
        translate([0,-(ctrArmL+ctrHeadL)/2,0]) {
            cutterArm();
        }
        cutterHead();
    }
}
module cutterArm() {
    cube([ctrArmW,ctrArmL, ctrHeadH], center=true);
}
module cutterHead() {
    union() {
        cube([ctrHeadW, ctrHeadL, ctrHeadH], center=true);
        translate([0, ctrHeadL/2-1, ctrHeadH/2-1]) 
        {  
            rotate([0,90,0])
            {
                cylinder(d=6, h=1, $fn=20, center=true);
            }
       }
   }
}
