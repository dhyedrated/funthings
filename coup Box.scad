in2mm = 25.4;
fudge = 1.01;
wall = 0.1 * in2mm;
cardW = 3.55*in2mm;
cardH = 2*in2mm;
card15Z = 0.5*in2mm;

botSet_totalW=(cardW+2*wall)*fudge;
botSet_totalHgt =(card15Z+wall) *fudge;


squareSide = 1.4*in2mm;
//bottom(false);
//piece();
/*TO WORK ON PLACEMENT
translate([cardH+2.25*wall,wall*fudge,wall*fudge]) {
    sqPiece();
}
translate([cardH+2.25*wall,(wall*fudge) + squareSide,wall*fudge]) {
    sqPiece();
}

translate([cardH+2.25*wall,(wall*fudge) + 2*squareSide,wall*fudge]) {
    piece();
}
//*/
//translate([125,0,0])
    top();
    
    
module bottom(solid) {
    
    union() {
        difference() {
            cube([botSet_totalW, botSet_totalW,botSet_totalHgt]);
            if (solid == false) {
                translate([wall,wall,wall]) {
                    cube([cardW, cardW,card15Z*2]);
                    
                }
            }            
       }
       if (solid == false) {
           translate([wall+cardH,wall*2,wall]) {
               cube([wall,cardW-wall*2,wall/2]);
           }
       }
   }
    
}

module top() {
    union() {
        difference() {
            
            difference() {
                cube([botSet_totalW+(2*wall*fudge)
                    ,botSet_totalW+(2*wall*fudge)
                    ,botSet_totalHgt+(wall *fudge)]);
                translate([wall,wall,0])
                    bottom(true);
            }
            translate([botSet_totalW/12,botSet_totalW/2.5,botSet_totalHgt]) {//wall/2]) {
                linear_extrude(height=3)
                    text(text="Coup",size=26);//, spacing=1,direction="ttb");
            }
        }
        //keep the parts of the O and p around
        translate([botSet_totalW/2.25,botSet_totalW/2.6,botSet_totalHgt+.5]) 
        {
            cube([2,21,wall-1]);
        }
        translate([botSet_totalW/1.17,botSet_totalW/2.6,botSet_totalHgt+.5]) 
        {
            cube([2,21,wall-1]);
        }
    }
}

module sqPiece() {
    color("red") {
    piece();
    rotate([0,0,90]) 
    {        
        translate([0,-squareSide,0])
            piece();
        rotate([0,0,90]) 
        {
            translate([-squareSide,-squareSide,0])
                piece();
            rotate([0,0,90]) 
            {
                translate([-squareSide,0,0])
                piece();
            }
        }
    }
}
}
module piece() {
    isoSide = (squareSide / sqrt(2))/sqrt(2);
    tri_pts =[[0,0],[squareSide*0.99,0],[isoSide*0.99,isoSide*0.99]];
    tri_ps =[[0,1,2]];
    difference() {
        linear_extrude(height = 1)
            polygon(tri_pts, tri_ps,10);
        
        translate([squareSide/2.5, isoSide/4.5,.1]) {
            linear_extrude(height=1.1)
                text(text="$",size=8);//, spacing=1,direction="ttb");
                
        }
    
    }
}
