//top lid

Odia = 42.1 ; //doh 38.1;
ID = 39.1;
lip = 2.1;

ventD = 10.95*1.1;
ventDp= 14.8;
shoulder =11.1;
arm = 16.5 + ventD/2;

//cylinder(d1=Odia, d2=37, h=lip*3.5, $fn=40);

difference() 
{
    cylinder(d=Odia, h=lip*3, $fn=36);//, center= true);
    translate([0,0,1.5*lip])
        cylinder(d2=ID, d1=ID-0.1, h=lip*1.51, $fn=36);//,center= true);
}

/*translate([0,50,0])
    plug();*/
    
  
  //arm();
  //spine();
module plug() {  
     cylinder(d1=ventD, d2= ventD/2, h=ventDp, $fn=36);
}


module arm() {
    cube([ventD, arm, 2]);
}
    
module spine() {
    difference(){
        cube([ventD,shoulder*2+Odia, 3], center=true);
        rotate([0,90,0])
            cylinder(d=2.5, h= ventD, center=true, $fn=36);
    }
}