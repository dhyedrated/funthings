diameter = 50;
thick = 5;
difference() {
    cylinder(d1=50, d2=49,h=thick, $fa=5);
    translate([11,5,13]) {
       sphere(10);
        cylinder(d=5,55, center =true,$fn=15);
    }
    translate([-13,-7,13]) {
        sphere(10);
        cylinder(d=5,55, center =true, $fn=15);
    }
}