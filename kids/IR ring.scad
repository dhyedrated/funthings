fingD = 22;
OD = 30;
ringW = 6;

wireChannel = 1.5;

ledRgD = 6.2;
ledRgH = 1.2;
ledD =5.2;
ledB = 10;

smallBatD =12;
smallBatH = 6.5;
LR44_negD = 6;
wall = wireChannel;// *2;

headD = 23;
headHgt = smallBatH+wall;
//ring();

/*
probably a pretty ugly and garnish ring.. but.. that could be 
designed around.. lol

TODOS: I'm thinking about putting a RED led in series as a 
resistor and to help S know that it's "working"
also.. as there will be trial and error i think I need to make a
twist lid for battery replacement

might need to print in 2-3 parts and glue head to ring piece.

idea was to have the palm side of the ring.. be split and have 
a little play.. a hard squeeze from your thumb.. and on..

//ring3();
difference() { //slice it in half    
    
    ring3();
    translate([-(OD+3)/2,-(OD+3)/2,0])
        cube([OD+3,OD+3,ringW+3]);
}
*/
   // head();
/*
translate([40, 0,0])
    project();

translate([0,-40,0])
    cover();

translate([0,40,0])
    head();
    
  */  
  d675 = 11.60;
  h675 = 5.4;
  neg675 = 3.7;
  $fn = 36;
  
  //lr675();
module lr675() {
    batStack();
    
}

module batStack() {    
    //cylinder(d=d675, h=h675, $fn, center=true);
    translate([0,0,h675]) {
        cylinder(d=d675, h=h675, $fn, center=true);
        translate([0,0,h675]) {
          //  cylinder(d=d675, h=h675, $fn, center=true);
        }
    }
}

module mark2internals() {
    lr675();
    
    translate([4,4.5,0])
        bat();
    translate([8, -4, 3.5]) {
        
        rotate([0,90,0])
                led();
    }
    translate([0,-4, 3.5]) {
        rotate([0,-90,0])
            led();
    }
    
}
    
    
module project() {  //jan 15 even better
  union() {
    head();
    difference() {
        translate([0,0,-OD/2.7])
            rotate([0,-90,0]) {            
                ring3();            
                
            }
        }
    }
}

module project2() {  //jan 14 looking pretty good
    union() {
        head();
        difference() {
            translate([0,0,-OD/2.7])
                rotate([0,90,0]) {
                
                    ring2();            
                    
                }
            
            cylinder(d=headD, h=headHgt, $fn=36);  //start of head
            plane();    
        }        
    }
//*/
}
module plane() {
    translate([-0,0,-23]) {
        rotate([45,0,0]) {
            cube([20,1,15], center=true);
        }
    }
}
module planeFlat() {
    translate([-11,-0,0]) {
        rotate([45,0,0]) {
            cube([20,1,15], center=true);
        }
    }
}

//lr32 and 1 LED  side set
module internals() {
    
    translate([4,4.5,0]){
        bat();
    }
    translate([8-ledD, -ledD, 3.5/2])
        cube([ledD,ledD,3.5]);
    translate([8, -4, 0])
    {          
        translate([0,0,3.5+(wall/2)]) //half wall.. trying to keep stuff even as i tweak because i lowered the internals so battery could push through
            rotate([0,90,0])
                led();
    }
   
    
}
//lr32 and 2 LED  not maintained double check
module internals2() {
    translate([4,4.5,0])
        bat();
    translate([8, -4, 3.5]) {
        
        rotate([0,90,0])
                led();
    }
    translate([0,-4, 3.5]) {
        rotate([0,-90,0])
            led();
    }
    
}
//lr32 and 1 LED not maintained double check
module internals3() {
    bat();
    translate([8, 0, 3.5]) {        
        rotate([0,90,0])
                led();
    }
}
module bat() {
    union() {
        cylinder (d=LR44_negD, h=smallBatH, $fn=36);
        translate([0,0,1]) {
            cylinder(d=smallBatD, h=smallBatH-1, $fn=36);
        }
        
    }
}
module led() {
    union() {
        cylinder(d=ledD, h=ledB, $fn=36);
        cylinder(d=ledRgD, h=ledRgH, $fn=36);
    }
}

module cover() {
    difference() {
        cylinder(d= headD+wall+1.5, h=headHgt+wall, $fn=8);
        headCore();
    }
}

         nubHgt = wall;

        nubD = 2;
        nubDHole = 2.8;
channelHole = nubDHole;// +0.5;
channelOffset = 2.25; //none of the above worked
//tried to make this share code and be elegant.. but the differnce 
//algorithm was betraying me
module headCore() {    
    nubFudge = 1.5; //not part of head construction
    union() {
        cylinder(d=headD, h=headHgt, $fn=36);
   
        translate([0,-headD /2,headHgt/2]) {
            rotate([90, 0,0])
                cylinder(d=nubDHole, h=nubHgt+nubFudge, $fn=36,center=true);
        }
        //this is gross
        translate([-wall/1.5,-headD /2,headHgt/2]) {
            rotate([90, 0,0])
                cylinder(d=nubDHole, h=nubHgt+nubFudge, $fn=36,center=true);
            
        }
       translate([-channelOffset,-headD /2,headHgt/3])
            cube([channelHole, nubDHole, headHgt/1.5 ], center= true);
        
        //right? side
        translate([0,headD /2,headHgt/2]) {
            rotate([90, 0,0])
                cylinder(d=nubDHole, h=nubHgt+nubFudge, $fn=36,center=true);
        }
        //this is gross even more
        translate([wall/1.5,headD /2,headHgt/2]) {
            rotate([90, 0,0])
                cylinder(d=nubDHole, h=nubHgt+nubFudge, $fn=36,center=true);
            
        }
       translate([channelOffset,headD /2,headHgt/3])
            cube([channelHole, nubDHole, headHgt/1.5 ], center= true);
        
        
        //led window.. from internals 1 (aka side set 1 LEd)
        translate([0,0,0]) {
            translate([8, -4, 3.5]) {
        cube([ledB, ledB, ledB], center=true);
        }
    }
        /*translate([-4,0,wall/2]) {
            translate([8, -4, 3.5]) {
        
            rotate([0,90,0])
                    led();
            }
        }*/
    }
}
module head() {
    union() {
        difference() {
            cylinder(d=headD, h=headHgt, $fn=36);
            translate([0,0,headHgt-5]) {
                cylinder(d=headD-wall, h=headHgt-wall, $fn=36);
            }
            translate([-4,0,0]) {//wall/2]) {
                internals();
            }
        }
  //  /*    //locking nubs
       
        translate([0,-headD /2,headHgt/2]) {
            rotate([90, 0,0])
                cylinder(d=nubD, h=nubHgt, $fn=36,center=true);
        }
        translate([0,headD /2,headHgt/2]) {
            rotate([90, 0,0])
                cylinder(d=nubD, h=nubHgt, $fn=36,center=true);
        }//*/
    }
    
    //Just battery box cylinder(d=smallBatD+ (wall*2), h=smallBatH*1.75, $fn=36);
}
//lr32 and 1 LED  not much different
module head2() {
    difference() {
        cylinder(d=headD, h=headHgt, $fn=36);
        translate([0,0,headHgt-5]) {
            cylinder(d=headD-wall, h=headHgt-wall, $fn=36);
        }
        translate([-4,0,wall/2]) {
            internals();
        }
    }
    
    //Just battery box cylinder(d=smallBatD+ (wall*2), h=smallBatH*1.75, $fn=36);
}

module ring() {
    difference() {
        cylinder(d=OD,h=6, center=true, $fn=36);
        cylinder(d=fingD, h=6,center=true, $fn=36);
        cylinder(d=OD-(OD-fingD)/2, h=wireChannel,center=true, $fn=36);
    }
}

//use rotation extrusion to have softer edges
module ring2() {
    difference() {
        rotate_extrude(convexity = 10, $fn = 36)
        translate([fingD/2 +(OD-fingD)/4, 0, 0])
        circle(r = (OD-fingD)/2.5, $fn = 36);
        cylinder(d=OD-(OD-fingD)/2, h=wireChannel,center=true, $fn=36);
    }
}

//use rotation extrusion to have softer edges
module ring3() {
    difference() {
        rotate_extrude(convexity = 10, $fn = 36)
        translate([fingD/2 +(OD-fingD)/4, 0, 0])
        circle(r = (OD-fingD)/2.5, $fn = 36);
        cylinder(d=OD-(OD-fingD)/2, h=wireChannel,center=true, $fn=36);
        planeFlat();
        translate([OD/1.5,0,])
        rotate([0,-90,0])
            cylinder(d=headD, h=headHgt, $fn=36);  //start of head
    }
}
