//Madelyns andriod
in2mm = 25.4;
width = 2*in2mm;
bodHgt = 2.5 * in2mm;

armW = .5*in2mm;
armH = 1*in2mm;

legW = .5*in2mm;
legH = legW;
///////////////////////
translate ([width *2, 0,0]) 
    head();

translate ([0,width *2, 0])
    body(); 
translate([width *2, width*2,0])
    arm();
translate([-width *2, -width*2,0])
    arm(); 
translate ([-width *2, 0,0])        
    leg();

translate ([0,-width *2, 0])
   leg();
   
/////////////
hpX = 0;
hpY = 0;
hpZ = bodHgt/2 + 2;
body();
translate([hpX,hpY,hpZ]) {
    rotate([0,0,-110])
        head();
}
lpX =( -legW) ;
lpY= 0;
lpZ = -hpZ - legH/2;
 translate([lpX,lpY,lpZ]) {
     leg();
 }
 translate([-lpX,lpY,lpZ]) {
     leg();
 }
 apX =( (width + armW) /2) ;
apY= 0;
apZ =0;
 translate([apX,apY,apZ]) {
     arm();
 }
 translate([-apX,apY,apZ]) {
     arm();
 }

 
   
module body() {
    //cube([width, width, bodHgt], center =true);
    cylinder(d=width, h=bodHgt,$fn=22, center=true);
}

module arm () {
    union() {
       // cube([armW,armW,armH],center=true);
        cylinder(d=armW, h=armH, $fn=20,center=true);
        
            translate ([0,0,-armH/2 ])//armW/2])
                sphere(d=armW, $fn=20);
            
        
    }
}
module leg() {
    //cube([legW,legW,legW]);
    union() {
       // cube([armW,armW,armH],center=true);
        cylinder(d=legW, h=legH, $fn=20,center=true);
        
        difference() {
            translate ([0,-legW/2,-legH/2 ])//armW/2])
                sphere(d=legW, $fn=20);
            translate([0,0,-legH]) {
                cube([legH*2, legH*20, legH], center=true);
            }
        }
    }
    
}

module head() {
    eye = .33 * in2mm;
    difference () {
        sphere(d=width);
        translate([0,0,-width/2]) {
            cube([width*2, width*20, width], center=true);
        }
        translate([width/2 - eye/2,0,width/3]) {
            sphere(d=eye, $fn=36);
        }
        translate([width/3 - eye/2,width/3,width/3]) {
            sphere(d=eye, $fn=36);
        }
    }
}