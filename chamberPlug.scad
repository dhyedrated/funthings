/*Base diameter	.226 in (5.7 mm)[1]
Rim diameter	.278 in (7.1 mm)[1]
Rim thickness	.043 in (1.1 mm)[1]
Case length	.613 in (15.6 mm)[1]
wikipedia
*/
// .22LR
/*
caseD = 5.6;
caseL = 15.6;
rimD = 7.1;
rimL = 1.1;
*/
/*410 wiki
Base diameter	.470 in (11.9 mm)
Rim diameter	.535 in (13.6 mm)
Rim thickness	.060 in (1.5 mm)
*/
caseD = 11.8;
caseL = 15;
rimD = 13.6;
rimL = 1.5;

eyelet = caseD/2;

union() {
    cylinder(d=caseD, h=caseL+rimL, $fn=30);    
    cylinder(d=rimD, h=rimL, $fn=30);  
    translate([0,eyelet/4,-eyelet/2.2]){
        rotate([90,0,0]) {
            difference() {
                cylinder(d=eyelet, h=eyelet/2, $fn=20);
                cylinder(d=2,h=caseD/2, $fn=20);
            }
        }
    }
}