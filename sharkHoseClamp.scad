ID = 45; // inner diameter and  height
hgt = ID; /// currently
lip =4;
wall =1.7;
wall2 = wall*2;
flap =14;
//rotate([-90,0,0]) {

//Bottom
//halfSleeve();

//top
module top() {
    union() {
        halfSleeve();
        translate([-ID*.335,-ID-.5,0])
        support();
    }
}
top();

module support() 
{
    supportHgt = 25;
    fudge =4;
    powerStack = 30 + fudge;
    cube([powerStack,supportHgt,hgt]);//, center=true);
    translate([-fudge,0,-hgt]){
        difference() {
            cube([powerStack+2*fudge,12,hgt*2]);
            translate([fudge,-.5,-.25])
                cube([powerStack,13,fudge*2]);
        }
    }
    //*/
}
/*
module supportCEnterStyle?() 
{
    supportHgt = 25;
    fudge =4;
    powerStack = 30 + fudge;
    cube([powerStack,supportHgt,hgt], center=true);
    translate([-fudge/2,13/2,-hgt/2]){
        difference() {
            cube([(powerStack+2*fudge)/2,12,hgt*2], center=true);
            translate([fudge/2,13/2,-.25])
                cube([powerStack,13,fudge*2], center=true);
        }
    }
    /
}*/
module halfSleeve() {
    difference() {
        union() {
            difference() {
                cylinder(d=ID+wall2, h=ID);
                cylinder(d=ID-lip, h=4);
                translate([0,0,1])
                {
                    cylinder(d=ID, h=ID-2);
                }
                translate([0,0,ID-lip])
                    cylinder(d=ID-lip, h=4);
            }
            translate([(ID+wall2)/2,0,2]) {
             //   difference() {
                    cube([flap,wall, ID-4]);
               // }
            }
            translate([-(ID+wall2)/2-flap,0,2]) {
                cube([flap,wall, ID-4]);
            }
        }
        translate([-ID/2-flap-wall,wall,0])
            cube([ID+(flap+wall2)*2,ID,ID]);
    }
}
