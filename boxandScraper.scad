//cube([30,30,30]);
/*steps =10;
            points = [
                for(x=[0:steps])[x,(pow(x,2)/10)]
                ];
                polygon(points);
*/

translate([50,0,0]) {
    
    difference() {
        union() {
            cube([50,50,5]);
            translate([50,5,0])
              rotate([0,-25,0])
                cube([50,40,5]);
        }   
        translate([0,25,-1]){
                cylinder(d=45, h = 7);        
        }
        translate([-1,-1,0]){
            rotate([0,-5,0]) {
                cube([50,55,5]);
            }
        }
    }
}

//cube([30,30,30]);

translate([-150,0,0]) {
    
    difference() {
        union() {
            cube([50,50,5]);
            translate([50,5,0])
              rotate([0,-25,0])
                cube([50,40,5]);
        }   
        translate([2,2,-1]){
               rotate([0,0,60.7]) {
                steps =60;
            points = [
                for(x=[-3:steps])[x,(pow((x-2),2)/100)]
                ];
                linear_extrude(height=10)
                    polygon(points); 
            }       
        }
        translate([-1,-1,0]){
            rotate([0,-5,0]) {
                cube([50,55,5]);
            }
        }
    }
}
//*/