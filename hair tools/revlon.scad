
    difference () {
        union() {
            cylinder(d=70, h=40);
            translate([-35,-35,0])
                cube([20,70,40]);
        }
        cylinder(d=40, h =40);
        
        translate([-15,-6,10])
            rotate([0,-90,0]) {
                cylinder(d=4, h=35, $fn=20);
                cylinder(d=9, h=7, $fn=20);
            }
        translate([-15,6,10])
            rotate([0,-90,0]) {
                cylinder(d=4, h=35, $fn=20);
                cylinder(d=9, h=7, $fn=20);
            }
        translate([20,0,0])
            cylinder(d=39, h =40);
        translate([0,0,52])
            sphere(d=70);
    }

