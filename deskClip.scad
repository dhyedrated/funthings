railGapX =17;
wall = 3;
railGapY = 30;

matGapX = 11;
matGapY = 20;//railGapY;//25;

hgt = 12.5;

difference()
{
    cube([wall+matGapX+wall+railGapX+wall,
            railGapY +wall, hgt]);
    translate([wall, 0,0])
        cube([matGapX, railGapY, hgt]);
    translate([wall+wall +matGapX, 0,0])
        cube([railGapX, railGapY, hgt]);
    cube([wall, railGapY-matGapY, hgt]);
}    
        
    