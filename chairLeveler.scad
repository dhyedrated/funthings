side = 15;
difference() {
union() 
{
    for( x=[0:1:10]) {
        step = x*1;
        translate([0,0,x*2])
        {
            cube([side+step,side+step,(side+step)/2], center=true);
        }
    }
}
cylinder(d=4, h=70, $fn=15, center=true);
}